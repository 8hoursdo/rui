module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var React = __webpack_require__(1);
	
	function rui() {
	}
	
	rui.version = '0.0.1';
	
	rui.render = function (component, container) {
	    if (typeof container === 'string') {
	        container = document.getElementById(container);
	    }
	
	    React.render(component, container);
	};
	
	
	rui.Dom = __webpack_require__(2);
	rui.TreeDataStore = __webpack_require__(3);
	
	rui.Application = __webpack_require__(4);
	rui.Button = __webpack_require__(5);
	rui.ButtonGroup = __webpack_require__(6);
	rui.Checkbox = __webpack_require__(7);
	rui.ProgressBar = __webpack_require__(8);
	rui.Radiobox = __webpack_require__(9);
	rui.Switch = __webpack_require__(10);
	rui.Textbox = __webpack_require__(11);
	rui.TreeNode = __webpack_require__(12);
	rui.TreeView = __webpack_require__(13);
	rui.MenuItem = __webpack_require__(14);
	rui.MenuDivider = __webpack_require__(15);
	rui.Menu = __webpack_require__(16);
	rui.FormActions = __webpack_require__(17);
	rui.FormDivider = __webpack_require__(18);
	rui.FormGroup = __webpack_require__(19);
	rui.Form = __webpack_require__(20);
	rui.Overlay = __webpack_require__(21);
	rui.ModalManager = __webpack_require__(22);
	rui.ModalFooter = __webpack_require__(23);
	rui.Modal = __webpack_require__(24);
	
	module.exports = rui;


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = require("react");

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	exports.clientWidth = function () {
	    return (window.innerWidth ||
	        window.document.documentElement.clientWidth ||
	        window.document.body.clientWidth);
	};
	
	exports.clientHeight = function () {
	    return (window.innerHeight ||
	        window.document.documentElement.clientHeight ||
	        window.document.body.clientHeight);
	}


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var utilities = __webpack_require__(25);
	
	function TreeNodeData(options) {
	    this.id = options.id;
	    this.text = options.text;
	    this.iconClassName = options.iconClassName;
	    this.data = options.data;
	    this.collapsed = options.collapsed;
	    this.checkable = options.checkable;
	    this.checked = options.checked;
	    this.children = options.children;
	    this.childrenNotLoaded = options.childrenNotLoaded;
	}
	
	function updateTreeNodeDataProperty(nodeData, options, propertyName) {
	    if (options.hasOwnProperty(propertyName)) {
	        nodeData[propertyName] = options[propertyName];
	    }
	}
	
	TreeNodeData.prototype.store = null;
	TreeNodeData.prototype.parent = null;
	
	TreeNodeData.prototype.id = null;
	TreeNodeData.prototype.text = null;
	TreeNodeData.prototype.iconClassName = null;
	TreeNodeData.prototype.data = null;
	TreeNodeData.prototype.collapsed = false;
	TreeNodeData.prototype.checkable = false;
	TreeNodeData.prototype.checked = false;
	TreeNodeData.prototype.children = null;
	TreeNodeData.prototype.childrenNotLoaded = false;
	
	TreeNodeData.prototype.update = function (options) {
	    updateTreeNodeDataProperty(this, options, 'text');
	    updateTreeNodeDataProperty(this, options, 'iconClassName');
	    updateTreeNodeDataProperty(this, options, 'data');
	    updateTreeNodeDataProperty(this, options, 'collapsed');
	    updateTreeNodeDataProperty(this, options, 'checkable');
	    updateTreeNodeDataProperty(this, options, 'checked');
	    updateTreeNodeDataProperty(this, options, 'children');
	    updateTreeNodeDataProperty(this, options, 'childrenNotLoaded');
	};
	
	TreeNodeData.prototype.setCollapsed = function (collapsed, recursive) {
	    this.collapsed = collapsed;
	
	    if (!recursive) {
	        return;
	    }
	
	    if (!this.store) {
	        return;
	    }
	
	    this.store.forEachChildNodeData(this, function (childNodeData) {
	        childNodeData.setCollapsed(collapsed, recursive);
	    });
	};
	
	function TreeDataStore(options) {
	    if (!options) {
	        options = {};
	    }
	
	    this._topNode = null;
	    this._nodes = [];
	    this._nodeMapById = {};
	
	    this.setTopNodeData(options.topNodeData || {});
	}
	
	function addNodeDataToStore(store, parentNode, node) {
	    var nodeId = node.id;
	    var nodeInStore = null;
	    if (nodeId) {
	        nodeInStore = store._nodeMapById[nodeId];
	    }
	
	    if (!nodeInStore) {
	        nodeInStore = new TreeNodeData(node);
	        if (nodeId) {
	            store._nodeMapById[nodeId] = nodeInStore;
	        }
	        nodeInStore.store = store;
	        nodeInStore.parent = parentNode;
	        store._nodes.push(nodeInStore);
	    } else {
	        nodeInStore.update(node);
	        nodeInStore.store = store;
	        nodeInStore.parent = parentNode;
	    }
	
	    if (node.children) {
	        nodeInStore.children = [];
	        node.children.forEach(function (childNode) {
	            nodeInStore.children.push(
	                addNodeDataToStore(store, nodeInStore, childNode));
	        });
	    }
	    return nodeInStore;
	}
	
	TreeDataStore.prototype.getTopNodeData = function () {
	    return this._topNode;
	};
	
	TreeDataStore.prototype.setTopNodeData = function (nodeData) {
	    this._nodes = [];
	    this._nodeMapById = {};
	
	    this._topNode = addNodeDataToStore(this, null, nodeData);
	    return this._topNode;
	};
	
	TreeDataStore.prototype.addNodeData = function (parentId, nodeData) {
	    var parentNode = this.getNodeDataById(parentId);
	    if (!parentNode) {
	        parentNode = addNodeDataToStore(this, null, {id: parentId});
	    }
	
	    return addNodeDataToStore(this, parentNode, nodeData);
	};
	
	TreeDataStore.prototype.getNodeDataById = function (id) {
	    if (!id) {
	        return this._topNode;
	    }
	
	    return this._nodeMapById[id];
	};
	
	TreeDataStore.prototype.getChildren = function (parentId) {
	    var parentNode = null;
	    if (typeof parentId === 'string') {
	        parentNode = this.getNodeDataById(parentId);
	    } else if (parentId instanceof TreeNodeData) {
	        parentNode = parentId;
	    }
	    
	    if (!parentNode) {
	        return [];
	    }
	
	    var children = parentNode.children;
	    if (!children) {
	        children = [];
	        this._nodes.forEach(function (eachNode) {
	            if (eachNode.parent === parentNode) {
	                children.push(eachNode);
	            }
	        });
	    }
	    return children;
	};
	
	TreeDataStore.prototype.forEachChildNodeData = function (parentId, iterator) {
	    var children = this.getChildren(parentId);
	    children.forEach(iterator);
	};
	
	module.exports = TreeDataStore;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var utilities = __webpack_require__(25);
	var global = __webpack_require__(26);
	
	var Application = React.createClass({displayName: "Application",
	    getInitialState: function () {
	        return {};
	    },
	
	    componentDidMount: function () {
	        global.setTheApplication(this);
	    },
	
	    render: function() {
	        return (
	            React.createElement("div", {className: "application"}, 
	                this.props.children
	            )
	        );
	    }
	});
	
	module.exports = Application;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var knownBrands = ['default', 'primary', 'success', 'info', 'warning', 'danger'];
	
	var Button = React.createClass({displayName: "Button",
	    render: function() {
	        var props = this.props,
	            type = props.type || 'button',
	            brand = props.brand || (type === 'submit' ? 'primary' : 'default'),
	            size = props.size,
	            disabled = !!props.disabled,
	            href = props.href,
	            className = 'btn';
	
	        if (knownBrands.indexOf(brand) !== -1) {
	            className += ' btn-' + brand;
	        }
	
	        if (size === 'large') {
	            className += ' btn-lg';
	        }
	
	        if (size === 'small') {
	            className += ' btn-sm';
	        }
	
	        if (size === 'mini') {
	            className += ' btn-xs';
	        }
	
	        if (props.active) {
	            className += ' active';
	        }
	
	        if (props.disabled) {
	            className += ' disabled';
	        }
	
	        if (href) {
	            // render as anchor
	            return (
	                React.createElement("a", React.__spread({},  this.props, {className: className, href: href}), props.text)
	            );
	        } else {
	            // render as button
	            return (
	                React.createElement("button", React.__spread({},  this.props, {className: className, disabled: disabled}), props.text)
	            );
	        }
	    }
	});
	
	module.exports = Button;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var Button = __webpack_require__(5);
	
	var ButtonGroup = React.createClass({displayName: "ButtonGroup",
	    render: function() {
	        var size = this.props.size,
	            className = 'btn-group';
	
	        if (size === 'large') {
	            className += ' btn-group-lg';
	        }
	
	        if (size === 'small') {
	            className += ' btn-group-sm';
	        }
	
	        if (size === 'mini') {
	            className += ' btn-group-xs';
	        }
	
	        return (
	            React.createElement("div", {className: className}, 
	                this.props.children
	            )
	        );
	    }
	});
	
	module.exports = ButtonGroup;


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var Checkbox = React.createClass({displayName: "Checkbox",
	    getInitialState: function () {
	        return {
	            checked: !!this.props.defaultChecked
	        };
	    },
	
	    render: function() {
	        var props = this.props,
	            name = props.name,
	            size = props.size,
	            readOnly = !!props.readOnly,
	            disabled = !!props.disabled,
	            className = 'chk';
	
	        if (size === 'large') {
	            className += ' chk-lg';
	        }
	
	        if (size === 'small') {
	            className += ' chk-sm';
	        }
	
	        if (readOnly) {
	            className += ' chk-readonly';
	        }
	
	        if (disabled) {
	            className += ' chk-disabled';
	        }
	
	        return (
	            React.createElement("div", {className: className}, 
	                React.createElement("label", null, 
	                    React.createElement("input", {type: "checkbox", name: name, checked: this.state.checked, readOnly: readOnly, disabled: disabled, value: props.value, onChange: this.handleChange}), 
	                    React.createElement("span", {className: "chk-text"}, props.text)
	                )
	            )
	        );
	    },
	
	    handleChange: function (event) {
	        if (this.props.readOnly) {
	            return;
	        }
	
	        this.setState({
	            checked : event.target.checked
	        });
	    }
	});
	
	module.exports = Checkbox;


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var PropTypes = React.PropTypes;
	
	function getPercentage(options) {
	    var delta = options.max - options.min;
	    if (delta) {
	        return Math.floor(options.value * 100 / delta);
	    }
	
	    return 0;
	}
	
	function getIndicateText(options) {
	    switch (options.indicator) {
	        case 'none':
	            return '';
	        case 'percentage':
	            return options.percentage + '%';
	        case 'progress':
	            return options.value + ' / ' + options.max;
	        case 'text':
	            var text;
	            if (options.percentage === 0) {
	                text = options.preparingText;
	            } else if (options.percentage === 100) {
	                text = options.completedText;
	            } else {
	                text = options.processingText;
	            }
	            return text;
	    }
	
	    return '';
	}
	
	var ProgressBar = React.createClass({displayName: "ProgressBar",
	    propTypes : {
	        min : PropTypes.number,
	        max : PropTypes.number,
	        value : PropTypes.number,
	        indicator : PropTypes.string
	    },
	
	    getDefaultProps : function () {
	        return {
	            min : 0,
	            max : 100,
	            value : 0,
	            indicator : 'percentage'
	        }
	    },
	
	    render: function() {
	        var props = this.props;
	
	        props.percentage = getPercentage(props);
	        var uncompletedPercentage = 100 - props.percentage;
	        var indicateText = getIndicateText(props);
	
	        var className = 'progress-bar';
	
	        return (
	            React.createElement("div", {className: className}, 
	                React.createElement("div", {className: "progress-bar-bg"}, indicateText), 
	                React.createElement("div", {className: "progress-bar-inner", style: {marginLeft: (uncompletedPercentage * -1) + '%'}}, 
	                    React.createElement("div", {className: "progress-bar-done", style: {marginLeft: uncompletedPercentage + '%'}}), 
	                    React.createElement("div", {className: "progress-bar-text", style: {marginLeft: uncompletedPercentage + '%'}}, indicateText)
	                )
	            )
	        );
	    }
	});
	
	module.exports = ProgressBar;


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var _radioboxesGroupByName = {};
	
	function addRadioboxIntoGroup(radiobox) {
	    var name = radiobox.props.name;
	    if (!name) {
	        return;
	    }
	
	    var radioboxes = _radioboxesGroupByName[name];
	    if (!radioboxes) {
	        _radioboxesGroupByName[name] = radioboxes = [];
	    }
	
	    radioboxes.push(radiobox);
	}
	
	function removeRadioboxFromGroup(radiobox) {
	    var name = radiobox.props.name;
	    if (!name) {
	        return;
	    }
	
	    var radioboxes = _radioboxesGroupByName[name];
	    if (!radioboxes) {
	        return;
	    }
	
	    var indexOfRadiobox = radioboxes.indexOf(radiobox);
	    if (indexOfRadiobox === -1) {
	        return;
	    }
	
	    radioboxes.splice(indexOfRadiobox, 1);
	}
	
	function getOtherCheckedRadiobox(radiobox) {
	    var name = radiobox.props.name;
	    if (!name) {
	        return null;
	    }
	
	    var radioboxes = _radioboxesGroupByName[name];
	    if (!radioboxes) {
	        return null;
	    }
	
	    for(var i = 0; i < radioboxes.length; i++) {
	        var eachRadiobox = radioboxes[i];
	        if (eachRadiobox.state.checked && eachRadiobox !== radiobox) {
	            return eachRadiobox;
	        }
	    }
	    return null;
	}
	
	var Radiobox = React.createClass({displayName: "Radiobox",
	    getInitialState: function () {
	        return {
	            checked: !!this.props.defaultChecked
	        };
	    },
	
	    componentDidMount: function () {
	        addRadioboxIntoGroup(this);
	    },
	
	    componentWillUnmount: function () {
	        removeRadioboxFromGroup(this);
	    },
	
	    render: function() {
	        var props = this.props,
	            name = props.name,
	            size = props.size,
	            readOnly = !!props.readOnly,
	            disabled = !!props.disabled,
	            className = 'rdo';
	
	        if (size === 'large') {
	            className += ' rdo-lg';
	        }
	
	        if (size === 'small') {
	            className += ' rdo-sm';
	        }
	
	        if (readOnly) {
	            className += ' rdo-readonly';
	        }
	
	        if (disabled) {
	            className += ' rdo-disabled';
	        }
	
	        console.log('rendering: checked=' + this.state.checked);
	        return (
	            React.createElement("div", {className: className}, 
	                React.createElement("label", null, 
	                    React.createElement("input", {type: "radio", name: name, checked: this.state.checked, readOnly: readOnly, disabled: disabled, value: props.value, onChange: this.handleChange, onClick: this.handleClick}), 
	                    React.createElement("span", {className: "rdo-text"}, props.text)
	                )
	            )
	        );
	    },
	
	    handleChange: function (event) {
	        var props = this.props,
	            name = props.name,
	            readOnly = props.readOnly;
	        var checked = event.target.checked;
	        if (readOnly) {
	            checked = this.state.checked;
	        }
	
	        if (name && checked) {
	            var otherCheckedbox = getOtherCheckedRadiobox(this);
	            if (otherCheckedbox) {
	                console.log('other checked box found');
	                otherCheckedbox.setState({checked : false});
	            }
	        }
	
	        this.setState({
	            checked : checked
	        });
	
	        return
	        if (readOnly) {
	            this.forceUpdate();
	        }
	    },
	
	    handleClick: function (event) {
	    }
	});
	
	module.exports = Radiobox;


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var Switch = React.createClass({displayName: "Switch",
	    getDefaultProps: function () {
	        return {
	            onText: 'YES',
	            offText: 'NO'
	        }
	    },
	
	    getInitialState: function () {
	        return {
	            checked: !!this.props.defaultChecked
	        };
	    },
	
	    render: function() {
	        var props = this.props,
	            className = 'switch';
	
	        if (this.state.checked) {
	            className += ' switch-checked';
	        }
	        if (props.disabled) {
	            className += ' switch-disabled';
	        }
	
	        return (
	            React.createElement("div", {className: className, onClick: this.handleClick}, React.createElement("a", {href: "javascript:;"}, React.createElement("span", {className: "switch-on"}, props.onText), React.createElement("span", {className: "switch-btn"}), React.createElement("span", {className: "switch-off"}, props.offText)))
	        );
	    },
	
	    handleClick: function (event) {
	        if (this.props.disabled) {
	            return;
	        }
	
	        this.setState({
	            checked: !this.state.checked
	        });
	    }
	});
	
	module.exports = Switch;


/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var knownTypes = ['text', 'password', 'email'];
	
	var Textbox = React.createClass({displayName: "Textbox",
	    getInitialState: function () {
	        return {
	            value: this.props.defaultValue
	        };
	    },
	
	    render: function() {
	        var props = this.props,
	            type = props.type,
	            name = props.name,
	            size = props.size,
	            readOnly = !!props.readOnly,
	            disabled = !!props.disabled,
	            className = 'input';
	
	        if (knownTypes.indexOf(type) === -1) {
	            type = 'text';
	        }
	
	        if (size === 'large') {
	            className += ' input-lg';
	        }
	
	        if (size === 'small') {
	            className += ' input-sm';
	        }
	
	        return (
	            React.createElement("input", React.__spread({},  this.props, {type: type, className: className, name: name, readOnly: readOnly, disabled: disabled, value: this.state.value, onChange: this.handleChange, onClick: this.handleClick}))
	        );
	    },
	
	    handleChange: function (event) {
	        this.setState({
	            value : event.target.value
	        });
	    },
	
	    handleClick: function (event) {
	        if (this.props.onClick) {
	            this.props.onClick(this, {event: event});
	        }
	    },
	
	    getValue: function () {
	        return this.state.value;
	    },
	
	    setValue: function (value) {
	        this.setState({value: value});
	    },
	
	    focus: function () {
	        if (!this.isMounted()) {
	            return;
	        }
	
	        this.getDOMNode().focus();
	    }
	});
	
	module.exports = Textbox;


/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var Utilities = __webpack_require__(25);
	
	var TreeNode = React.createClass({displayName: "TreeNode",
	    getInitialState: function () {
	        return {
	            collapsed : !!this.props.defaultCollapsed
	        };
	    },
	
	    render: function() {
	        var props = this.props;
	        var nodeData = this.props.nodeData;
	
	        var text = props.text;
	        if (nodeData) {
	            text = nodeData.text;
	        }
	
	        var level = props.level || 0;
	        var children = props.children;
	        if (nodeData) {
	            children = [];
	            var dataStore = nodeData.store;
	            dataStore.forEachChildNodeData(nodeData, function (childNodeData) {
	                children.push(React.createElement(TreeNode, {key: childNodeData.id, nodeData: childNodeData, treeView: props.treeView, level: level + 1}));
	            });
	            props.children = children;
	        } else {
	            children.forEach(function (child) {
	                child.props.level = level + 1;
	                child.props.treeView = props.treeView;
	            });
	        }
	        this.childTreeNodes = children;
	
	        var childrenNotLoaded = props.childrenNotLoaded;
	        if (nodeData) {
	            props.childrenNotLoaded = childrenNotLoaded = nodeData.childrenNotLoaded;
	        }
	
	        var collapsed = this.isCollapsed();
	        var className = 'tree-node';
	        var treeNodeBodyClassName = 'tree-node-body';
	        var treeNodeChildrenClassName = 'tree-node-children';
	
	        if (this.isActiveNode()) {
	            className += ' tree-node-active';
	            treeNodeBodyClassName += ' tree-node-body-active';
	            treeNodeChildrenClassName += ' tree-node-children-active';
	        }
	
	        var expanderText;
	        if (this.isLeafNode()) {
	            className += ' tree-node-leaf';
	            treeNodeBodyClassName += ' tree-node-body-leaf';
	            treeNodeChildrenClassName += ' tree-node-children-leaf';
	            expanderText = '◦';
	        } else {
	            if (collapsed) {
	                className += ' tree-node-collapsed';
	                treeNodeBodyClassName += ' tree-node-body-collapsed';
	                treeNodeChildrenClassName += ' tree-node-children-collapsed';
	                expanderText = '▸';
	            } else {
	                expanderText = '▾';
	            }
	        }
	
	        var hideTopNode = props.treeView.props.hideTopNode;
	        if (level === 0) {
	            treeNodeBodyClassName += ' tree-node-body-top';
	            if (hideTopNode) {
	                treeNodeBodyClassName += ' tree-node-body-top-hide';
	            }
	        }
	
	        var nodeIndentComponents = [];
	        var indentCount = level;
	        if (hideTopNode) {
	            indentCount -= 1;
	        }
	        for (var i = 0; i < indentCount; i++) {
	            nodeIndentComponents.push(React.createElement("div", {className: "tree-node-indent"}));
	        }
	
	        return (
	            React.createElement("div", {className: className}, 
	                React.createElement("div", {className: treeNodeBodyClassName, onClick: this.handleNodeBodyClick}, 
	                    React.createElement("div", {className: "tree-node-indents"}, nodeIndentComponents), 
	                    React.createElement("div", {className: "tree-node-expander", onClick: this.handleNodeExpanderClick}, expanderText), 
	                    React.createElement("div", {className: "tree-node-content"}, 
	                        React.createElement("span", {className: "tree-node-label"}, text)
	                    )
	                ), 
	                React.createElement("div", {className: treeNodeChildrenClassName}, children)
	            )
	        );
	    },
	
	    handleNodeBodyClick : function (event) {
	        var treeView = this.props.treeView;
	        treeView.setActiveNode(this);
	        if (this.props.onClick) {
	            this.props.onClick(this);
	        }
	        treeView.notifyNodeClick(this);
	    },
	
	    handleNodeExpanderClick : function (event) {
	        var props = this.props;
	
	        if (!this.isLeafNode()) {
	            var collapsed = this.isCollapsed();
	            if (collapsed) {
	                this.expand();
	            } else {
	                this.collapse();
	            }
	        }
	        event.stopPropagation();
	    },
	
	    expand: function () {
	        if (this.isExpanded()) {
	            return;
	        }
	
	        var props = this.props;
	        var treeView = props.treeView;
	
	        var args = {};
	        if (props.beforeExpand) {
	            props.beforeExpand(this, args);
	            if (args.cancel) {
	                return;
	            }
	        }
	        treeView.notifyBeforeNodeExpand(this, args);
	        if (args.cancel) {
	            return;
	        }
	
	        var nodeData = props.nodeData;
	        if (nodeData) {
	            nodeData.collapsed = false;
	        }
	        this.setState({
	            collapsed : false
	        });
	
	        if (props.onExpand) {
	            props.onExpand(this);
	        }
	        treeView.notifyNodeExpand(this);
	    },
	
	    collapse: function (recursive) {
	        if (this.isCollapsed()) {
	            return;
	        }
	
	        var props = this.props;
	        var treeView = props.treeView;
	
	        var args = {};
	        if (props.beforeCollapse) {
	            props.beforeCollapse(this, args);
	            if (args.cancel) {
	                return;
	            }
	        }
	        treeView.notifyBeforeNodeCollapse(this, args);
	        if (args.cancel) {
	            return;
	        }
	
	        var nodeData = props.nodeData;
	        if (nodeData) {
	            nodeData.setCollapsed(true, recursive);
	        }
	        this.setState({
	            collapsed : true
	        });
	
	        if (props.onCollapse) {
	            props.onCollapse(this);
	        }
	        treeView.notifyNodeCollapse(this);
	    },
	
	    isExpanded: function () {
	        return !this.isCollapsed();
	    },
	
	    isCollapsed: function () {
	        var collapsed = this.state.collapsed;
	        var nodeData = this.props.nodeData;
	        if (nodeData) {
	            collapsed = nodeData.collapsed;
	        }
	        return collapsed;
	    },
	
	    isLeafNode : function () {
	        var props = this.props;
	        return !props.childrenNotLoaded && (!props.children || !props.children.length);
	    },
	
	    isActiveNode : function () {
	        return this.props.treeView.getActiveNode() === this;
	    },
	
	    getText : function () {
	        var props = this.props;
	        var text = props.text;
	        var nodeData = props.nodeData;
	        if (nodeData) {
	            text = nodeData.text;
	        }
	        return text;
	    }
	});
	
	module.exports = TreeNode;


/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var TreeNode = __webpack_require__(12);
	
	var TreeView = React.createClass({displayName: "TreeView",
	    getInitialState: function () {
	        return {
	            hideTopNode : this.props.hideTopNode,
	            dataStore : this.props.dataStore
	        };
	    },
	
	    render: function() {
	        var topNodeData = this.state.dataStore.getTopNodeData();
	        return (
	            React.createElement("div", {className: "tree-view"}, 
	                React.createElement(TreeNode, {ref: "topTreeNode", key: topNodeData.id, nodeData: topNodeData, treeView: this, level: 0})
	            )
	        );
	    },
	
	    getTopTreeNode: function() {
	        return this.refs.topTreeNode;
	    },
	
	    getActiveNode: function () {
	        return this.activeNode;
	    },
	
	    setActiveNode: function (activeNode) {
	        if (this.activeNode === activeNode) {
	            return;
	        }
	
	        var oldActiveNode = this.activeNode;
	        this.activeNode = activeNode;
	        if (oldActiveNode) {
	            oldActiveNode.forceUpdate();
	        }
	        if (activeNode) {
	            activeNode.forceUpdate();
	        }
	
	        if (this.props.onActiveNodeChange) {
	            this.props.onActiveNodeChange(this, {
	                activeNode: activeNode,
	                oldActiveNode: oldActiveNode
	            });
	        }
	    },
	
	    notifyNodeClick: function (node) {
	        if (this.props.onNodeClick) {
	            this.props.onNodeClick(this, {
	                node: node
	            });
	        }
	    },
	
	    notifyBeforeNodeCollapse: function (node, args) {
	        if (this.props.beforeNodeCollapse) {
	            var evtArgs = {
	                    node: node
	                };
	            this.props.beforeNodeCollapse(this, evtArgs);
	            args.cancel = evtArgs.cancel;
	        }
	    },
	
	    notifyNodeCollapse: function (node) {
	        if (this.props.onNodeCollapse) {
	            this.props.onNodeCollapse(this, {
	                node: node
	            });
	        }
	    },
	
	    notifyBeforeNodeExpand: function (node, args) {
	        if (this.props.beforeNodeExpand) {
	            var evtArgs = {
	                    node: node
	                };
	            this.props.beforeNodeExpand(this, evtArgs);
	            args.cancel = evtArgs.cancel;
	        }
	    },
	
	    notifyNodeExpand: function (node) {
	        if (this.props.onNodeExpand) {
	            this.props.onNodeExpand(this, {
	                node: node
	            });
	        }
	    }
	});
	
	module.exports = TreeView;


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var MenuItem = React.createClass({displayName: "MenuItem",
	    render: function() {
	        return (
	            React.createElement("li", {className: "menu-item"}, 
	                React.createElement("a", {onClick: this.handleMenuItemClick}, 
	                    React.createElement("i", null), 
	                    this.props.text
	                )
	            )
	        );
	    },
	
	    handleMenuItemClick: function (event) {
	        if (this.props.onClick) {
	            this.props.onClick(this);
	        }
	        event.preventDefault();
	        event.stopPropagation();
	
	        this.props.menu.notifyMenuItemClick(this);
	    }
	});
	
	module.exports = MenuItem;


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var MenuDivider = React.createClass({displayName: "MenuDivider",
	    render: function() {
	        return (
	            React.createElement("li", {className: "menu-divider"})
	        );
	    }
	});
	
	module.exports = MenuDivider;


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var utilities = __webpack_require__(25);
	
	var Menu = React.createClass({displayName: "Menu",
	    getInitialState: function () {
	        var props = this.props;
	        var visible = true;
	        if (!utilities.isUndefined(props.defaultVisible)) {
	            visible = props.defaultVisible;
	        }
	
	        var top = 0;
	        if (!utilities.isUndefined(props.defaultTop)) {
	            top = props.defaultTop;
	        }
	
	        var left = 0;
	        if (!utilities.isUndefined(props.defaultLeft)) {
	            left = props.defaultLeft;
	        }
	
	        return {
	            visible: visible,
	            top: top,
	            left: left
	        };
	    },
	
	    render: function() {
	        var me = this;
	        var props = this.props;
	        var children = props.children;
	
	        if (children && children.length) {
	            children.forEach(function (child) {
	                child.props.menu = me;
	            });
	        }
	
	        var state = this.state;
	        var style = {
	            top: state.top || 0,
	            left: state.left || 0
	        };
	        if (!this.state.visible) {
	            style.display = 'none';
	        }
	
	        return (
	            React.createElement("div", {className: "menu", style: style, onMouseDown: this.handleMouseDown}, 
	                React.createElement("div", {className: "menu-items"}, 
	                    children
	                )
	            )
	        );
	    },
	
	    handleMouseDown: function (event) {
	        event.stopPropagation();
	    },
	
	    notifyMenuItemClick: function (menuItem) {
	        this.setState({
	            visible: false
	        });
	
	        if (this.props.onItemClick) {
	            this.props.onItemClick(this, {
	                item: menuItem
	            });
	        }
	    },
	
	    popup: function (options) {
	        var state = {
	            visible: true
	        };
	
	        if (!utilities.isUndefined(options.top)) {
	            state.top = options.top;
	        }
	        if (!utilities.isUndefined(options.left)) {
	            state.left = options.left;
	        }
	
	        this.setState(state);
	    },
	
	    close: function () {
	        this.setState({
	            visible: false
	        });
	    }
	});
	
	module.exports = Menu;


/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var FormActions = React.createClass({displayName: "FormActions",
	    render: function() {
	        var className = 'form-actions';
	        var align = this.props.align;
	        if (align === 'center') {
	            className += ' form-actions-align-center';
	        } else if (align === 'right') {
	            className += ' form-actions-align-right';
	        } else if (align === 'left') {
	            className += ' form-actions-align-left';
	        }
	        return (
	            React.createElement("div", {className: className}, 
	                this.props.children
	            )
	        );
	    }
	});
	
	module.exports = FormActions;


/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var FormDivider = React.createClass({displayName: "FormDivider",
	    render: function() {
	        return (
	            React.createElement("div", {className: "form-divider"})
	        );
	    }
	});
	
	module.exports = FormDivider;


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var FormGroup = React.createClass({displayName: "FormGroup",
	    render: function() {
	        var props = this.props;
	        var className = 'form-group';
	
	        if (props.stacked) {
	            className += ' form-group-stacked';
	        }
	
	        if (props.inline) {
	            className += ' form-group-inline';
	        }
	
	        if (props.nolabel) {
	            className += ' form-group-nolabel';
	        }
	
	        if (props.compact) {
	            className += ' form-group-compact';
	        }
	
	        if (props.first) {
	            className += ' form-group-first';
	        }
	
	        return (
	            React.createElement("div", {className: className}, 
	                React.createElement("label", {className: "form-label"}, props.label), 
	                React.createElement("div", {className: "form-controls"}, 
	                    props.children
	                )
	            )
	        );
	    }
	});
	
	module.exports = FormGroup;


/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var Form = React.createClass({displayName: "Form",
	    render: function() {
	        var className = 'form';
	        if (this.props.modal) {
	            className += ' form-modal';
	        }
	
	        if (this.props.narrowLabel) {
	            className += ' form-narrow-label';
	        }
	
	        if (this.props.stackedLabel) {
	            className += ' form-stacked-label';
	        }
	
	        return (
	            React.createElement("form", React.__spread({},  this.props, {className: className, onSubmit: this.handleSubmit}), 
	                this.props.children
	            )
	        );
	    },
	
	    handleSubmit: function (event) {
	        if (this.props.onSubmit) {
	            this.props.onSubmit(this, {event: event});
	        }
	    }
	});
	
	module.exports = Form;


/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var utilities = __webpack_require__(25);
	
	var MIN_OVERLAY_Z_INDEX = 2300;
	
	var Overlay = React.createClass({displayName: "Overlay",
	    getInitialState: function () {
	        var zIndex = this.props.defaultZIndex || MIN_OVERLAY_Z_INDEX;
	        return {
	            zIndex: zIndex
	        };
	    },
	
	    render: function() {
	        var zIndex = this.state.zIndex;
	        var style = {
	            zIndex: this.state.zIndex
	        };
	
	        if (zIndex === MIN_OVERLAY_Z_INDEX) {
	            style.display = 'none';
	        }
	
	        return (
	            React.createElement("div", {className: "overlay", style: style, onClick: this.handleClick})
	        );
	    },
	
	    handleClick: function (event) {
	        if (this.props.onClick) {
	            this.props.onClick(this);
	        }
	        event.stopPropagation();
	    },
	
	    show: function () {
	        var state = {
	            zIndex: this.state.zIndex + 2
	        };
	        this.setState(state);
	        return state.zIndex;
	    },
	
	    hide: function () {
	        var zIndex = this.state.zIndex;
	        if (zIndex === MIN_OVERLAY_Z_INDEX) {
	            return;
	        }
	
	        var state = {
	            zIndex: zIndex - 2
	        };
	        this.setState(state);
	    }
	});
	
	module.exports = Overlay;


/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var Dom = __webpack_require__(2);
	var utilities = __webpack_require__(25);
	var global = __webpack_require__(26);
	var Overlay = __webpack_require__(21);
	
	var theModalManager = null;
	
	var ModalManager = React.createClass({displayName: "ModalManager",
	    getInitialState: function () {
	        return {
	            modals: []
	        };
	    },
	
	    render: function() {
	        return (
	            React.createElement("div", {className: "modal-manager"}, 
	                React.createElement(Overlay, {ref: "overlay"})
	            )
	        );
	    },
	
	    componentDidMount: function () {
	        theModalManager = this;
	    },
	
	    componentWillUnmount: function () {
	        if (theModalManager === this) {
	            theModalManager = null;
	        }
	    },
	
	    showOverlay: function () {
	        return this.refs.overlay.show();
	    },
	
	    hideOverlay: function () {
	        this.refs.overlay.hide();
	    },
	
	    showModal: function (modal, options, callback) {
	        var zIndex = this.showOverlay();
	        options.zIndex = zIndex + 1;
	        modal.show(options, function () {
	            var left = options.left;
	            var top = options.top;
	
	            var modalDOMNode = modal.getDOMNode();
	            if (utilities.isUndefined(left)) {
	                var clientWidth = Dom.clientWidth();
	                var modalWidth = modalDOMNode.offsetWidth;
	                var scrollLeft = document.documentElement.scrollLeft;
	                left = (clientWidth - modalWidth) * 0.5 + scrollLeft;
	            }
	
	            if (utilities.isUndefined(top)) {
	                var clientHeight = Dom.clientHeight();
	                var modalHeight = modalDOMNode.offsetHeight;
	                var scrollTop = document.documentElement.scrollTop;
	                top = (clientHeight - modalHeight) * 0.4 + scrollTop;
	            }
	
	            if (left < 0) {
	                left = 0;
	            }
	
	            if (top < 0) {
	                top = 0;
	            }
	
	            var nextState = {
	                left: left,
	                top: top
	            }
	            modal.setState(nextState, callback);
	        });
	    }
	});
	
	ModalManager.showModal = function (modal, options) {
	    if (!theModalManager) {
	        throw new Error("Can't show modal. You must put a ModalManager component in your components.");
	    }
	
	    return theModalManager.showModal(modal, options);
	};
	
	ModalManager.closeModal = function (modal) {
	    var closed = modal.close();
	    if (closed === false) {
	        return;
	    }
	
	    if (theModalManager) {
	        theModalManager.hideOverlay();
	    }
	};
	
	module.exports = ModalManager;


/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	
	var ModalFooter = React.createClass({displayName: "ModalFooter",
	    render: function() {
	        return (
	            React.createElement("div", {className: "modal-footer"}, 
	                this.props.children
	            )
	        );
	    }
	});
	
	module.exports = ModalFooter;


/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * @jsx React.DOM
	 */
	var React = __webpack_require__(1);
	var utilities = __webpack_require__(25);
	var ModalManager = __webpack_require__(22);
	var ModalFooter = __webpack_require__(23);
	var Form = __webpack_require__(20);
	
	var Modal = React.createClass({displayName: "Modal",
	    getInitialState: function () {
	        var props = this.props;
	        return {
	            visible: props.defaultVisible || false,
	            width: props.defaultWidth || 'auto',
	            height: props.defaultHeight || 'auto',
	            left: props.defaultLeft || 0,
	            top: props.defaultTop || 0,
	            zIndex: props.defaultZIndex || 2400
	        };
	    },
	
	    render: function() {
	        var state = this.state;
	        var style = {
	            width: state.width,
	            height: state.height,
	            top: state.top,
	            left: state.left,
	            zIndex: state.zIndex
	        };
	
	        if (!this.state.visible) {
	            style.display = 'none';
	        }
	
	        var header = (
	            React.createElement("div", {key: "header", className: "modal-header"}, 
	                React.createElement("button", {type: "button", className: "close", onClick: this.handleCloseBtnClick}, "×"), 
	                React.createElement("h4", {className: "modal-title"}, this.props.title)
	            )
	        );
	        var bodyChildren = [];
	        var footer = null;
	        React.Children.forEach(this.props.children, function (child) {
	            if (child.type === ModalFooter.type) {
	                footer = child;
	                footer.props.key = 'footer';
	            } else {
	                if (child.type === Form.type) {
	                    if (utilities.isUndefined(child.props.modal)) {
	                        child.props.modal = true;
	                    }
	                }
	                bodyChildren.push(child);
	            }
	        });
	
	        var body = (
	            React.createElement("div", {key: "body", className: "modal-body"}, 
	                React.createElement("div", {className: "modal-body-inner"}, 
	                    bodyChildren
	                )
	            )
	        );
	
	        var modalChildren = [header, body];
	        if (footer) {
	            modalChildren.push(footer);
	        }
	
	        return (
	            React.createElement("div", {className: "modal-dialog", style: style, tabIndex: -1, onKeyDown: this.handleKeyDown}, 
	                modalChildren
	            )
	        );
	    },
	
	    handleKeyDown: function (event) {
	        if (event.key === 'Escape') {
	            this.close();
	        }
	        event.stopPropagation();
	    },
	
	    handleCloseBtnClick: function (event) {
	        event.stopPropagation();
	        this.close();
	    },
	
	    show: function (options, callback) {
	        if (this.state.visible) {
	            return;
	        }
	
	        if (!options) {
	            options = {};
	        }
	
	        var state = {
	            visible: true,
	        };
	
	        if (!utilities.isUndefined(options.zIndex)) {
	            state.zIndex = options.zIndex;
	        }
	
	        var self = this;
	        this.setState(state, function () {
	            callback();
	
	            if (self.props.onActive) {
	                self.props.onActive(self);
	            }
	        });
	    },
	
	    place: function (options, callback) {
	        var state = {};
	
	        if (!utilities.isUndefined(options.top)) {
	            state.top = options.top;
	        }
	        if (!utilities.isUndefined(options.left)) {
	            state.left = options.left;
	        }
	        this.setState(state, callback);
	    },
	
	    showModal: function (options, callback) {
	        if (!options) {
	            options = {};
	        }
	
	        this._showAsModal = true;
	        ModalManager.showModal(this, options, callback);
	    },
	
	    close: function () {
	        if (!this.state.visible) {
	            return;
	        }
	
	        if (this._showAsModal) {
	            this._showAsModal = false;
	            ModalManager.closeModal(this);
	            return;
	        }
	
	        var state = {
	            visible: false
	        };
	        this.setState(state);
	        if (this.props.onClose) {
	            this.props.onClose(this);
	        }
	    }
	});
	
	module.exports = Modal;


/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	exports.isUndefined = function (obj) {
	    return typeof obj === 'undefined';
	};


/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	var theApp;
	
	exports.setTheApplication = function (app) {
	    theApp = app;
	};
	
	exports.getTheApplication = function () {
	    return theApp;
	}


/***/ }
/******/ ])
//# sourceMappingURL=rui.commonjs.js.map