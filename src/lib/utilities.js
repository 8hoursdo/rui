exports.isUndefined = function (obj) {
    return typeof obj === 'undefined';
};
