var utilities = require('./utilities');

function TreeNodeData(options) {
    this.id = options.id;
    this.text = options.text;
    this.iconClassName = options.iconClassName;
    this.data = options.data;
    this.collapsed = options.collapsed;
    this.checkable = options.checkable;
    this.checked = options.checked;
    this.children = options.children;
    this.childrenNotLoaded = options.childrenNotLoaded;
}

function updateTreeNodeDataProperty(nodeData, options, propertyName) {
    if (options.hasOwnProperty(propertyName)) {
        nodeData[propertyName] = options[propertyName];
    }
}

TreeNodeData.prototype.store = null;
TreeNodeData.prototype.parent = null;

TreeNodeData.prototype.id = null;
TreeNodeData.prototype.text = null;
TreeNodeData.prototype.iconClassName = null;
TreeNodeData.prototype.data = null;
TreeNodeData.prototype.collapsed = false;
TreeNodeData.prototype.checkable = false;
TreeNodeData.prototype.checked = false;
TreeNodeData.prototype.children = null;
TreeNodeData.prototype.childrenNotLoaded = false;

TreeNodeData.prototype.update = function (options) {
    updateTreeNodeDataProperty(this, options, 'text');
    updateTreeNodeDataProperty(this, options, 'iconClassName');
    updateTreeNodeDataProperty(this, options, 'data');
    updateTreeNodeDataProperty(this, options, 'collapsed');
    updateTreeNodeDataProperty(this, options, 'checkable');
    updateTreeNodeDataProperty(this, options, 'checked');
    updateTreeNodeDataProperty(this, options, 'children');
    updateTreeNodeDataProperty(this, options, 'childrenNotLoaded');
};

TreeNodeData.prototype.setCollapsed = function (collapsed, recursive) {
    this.collapsed = collapsed;

    if (!recursive) {
        return;
    }

    if (!this.store) {
        return;
    }

    this.store.forEachChildNodeData(this, function (childNodeData) {
        childNodeData.setCollapsed(collapsed, recursive);
    });
};

function TreeDataStore(options) {
    if (!options) {
        options = {};
    }

    this._topNode = null;
    this._nodes = [];
    this._nodeMapById = {};

    this.setTopNodeData(options.topNodeData || {});
}

function addNodeDataToStore(store, parentNode, node) {
    var nodeId = node.id;
    var nodeInStore = null;
    if (nodeId) {
        nodeInStore = store._nodeMapById[nodeId];
    }

    if (!nodeInStore) {
        nodeInStore = new TreeNodeData(node);
        if (nodeId) {
            store._nodeMapById[nodeId] = nodeInStore;
        }
        nodeInStore.store = store;
        nodeInStore.parent = parentNode;
        store._nodes.push(nodeInStore);
    } else {
        nodeInStore.update(node);
        nodeInStore.store = store;
        nodeInStore.parent = parentNode;
    }

    if (node.children) {
        nodeInStore.children = [];
        node.children.forEach(function (childNode) {
            nodeInStore.children.push(
                addNodeDataToStore(store, nodeInStore, childNode));
        });
    }
    return nodeInStore;
}

TreeDataStore.prototype.getTopNodeData = function () {
    return this._topNode;
};

TreeDataStore.prototype.setTopNodeData = function (nodeData) {
    this._nodes = [];
    this._nodeMapById = {};

    this._topNode = addNodeDataToStore(this, null, nodeData);
    return this._topNode;
};

TreeDataStore.prototype.addNodeData = function (parentId, nodeData) {
    var parentNode = this.getNodeDataById(parentId);
    if (!parentNode) {
        parentNode = addNodeDataToStore(this, null, {id: parentId});
    }

    return addNodeDataToStore(this, parentNode, nodeData);
};

TreeDataStore.prototype.getNodeDataById = function (id) {
    if (!id) {
        return this._topNode;
    }

    return this._nodeMapById[id];
};

TreeDataStore.prototype.getChildren = function (parentId) {
    var parentNode = null;
    if (typeof parentId === 'string') {
        parentNode = this.getNodeDataById(parentId);
    } else if (parentId instanceof TreeNodeData) {
        parentNode = parentId;
    }
    
    if (!parentNode) {
        return [];
    }

    var children = parentNode.children;
    if (!children) {
        children = [];
        this._nodes.forEach(function (eachNode) {
            if (eachNode.parent === parentNode) {
                children.push(eachNode);
            }
        });
    }
    return children;
};

TreeDataStore.prototype.forEachChildNodeData = function (parentId, iterator) {
    var children = this.getChildren(parentId);
    children.forEach(iterator);
};

module.exports = TreeDataStore;
