var React = require('react');

function rui() {
}

rui.version = '0.0.1';

rui.render = function (component, container) {
    if (typeof container === 'string') {
        container = document.getElementById(container);
    }

    React.render(component, container);
};


rui.Dom = require('./dom');
rui.TreeDataStore = require('./tree-data-store');

rui.Application = require('./components/application.jsx');
rui.Button = require('./components/button.jsx');
rui.ButtonGroup = require('./components/button-group.jsx');
rui.Checkbox = require('./components/checkbox.jsx');
rui.ProgressBar = require('./components/progress-bar.jsx');
rui.Radiobox = require('./components/radiobox.jsx');
rui.Switch = require('./components/switch.jsx');
rui.Textbox = require('./components/textbox.jsx');
rui.TreeNode = require('./components/tree-node.jsx');
rui.TreeView = require('./components/tree-view.jsx');
rui.MenuItem = require('./components/menu-item.jsx');
rui.MenuDivider = require('./components/menu-divider.jsx');
rui.Menu = require('./components/menu.jsx');
rui.FormActions = require('./components/form-actions.jsx');
rui.FormDivider = require('./components/form-divider.jsx');
rui.FormGroup = require('./components/form-group.jsx');
rui.Form = require('./components/form.jsx');
rui.Overlay = require('./components/overlay.jsx');
rui.ModalManager = require('./components/modal-manager.jsx');
rui.ModalFooter = require('./components/modal-footer.jsx');
rui.Modal = require('./components/modal.jsx');

module.exports = rui;
