exports.clientWidth = function () {
    return (window.innerWidth ||
        window.document.documentElement.clientWidth ||
        window.document.body.clientWidth);
};

exports.clientHeight = function () {
    return (window.innerHeight ||
        window.document.documentElement.clientHeight ||
        window.document.body.clientHeight);
}
