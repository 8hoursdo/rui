var theApp;

exports.setTheApplication = function (app) {
    theApp = app;
};

exports.getTheApplication = function () {
    return theApp;
}
