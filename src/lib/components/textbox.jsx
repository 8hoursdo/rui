/**
 * @jsx React.DOM
 */
var React = require('react');

var knownTypes = ['text', 'password', 'email'];

var Textbox = React.createClass({
    getInitialState: function () {
        return {
            value: this.props.defaultValue
        };
    },

    render: function() {
        var props = this.props,
            type = props.type,
            name = props.name,
            size = props.size,
            readOnly = !!props.readOnly,
            disabled = !!props.disabled,
            className = 'input';

        if (knownTypes.indexOf(type) === -1) {
            type = 'text';
        }

        if (size === 'large') {
            className += ' input-lg';
        }

        if (size === 'small') {
            className += ' input-sm';
        }

        return (
            <input {...this.props} type={type} className={className} name={name} readOnly={readOnly} disabled={disabled} value={this.state.value} onChange={this.handleChange} onClick={this.handleClick} />
        );
    },

    handleChange: function (event) {
        this.setState({
            value : event.target.value
        });
    },

    handleClick: function (event) {
        if (this.props.onClick) {
            this.props.onClick(this, {event: event});
        }
    },

    getValue: function () {
        return this.state.value;
    },

    setValue: function (value) {
        this.setState({value: value});
    },

    focus: function () {
        if (!this.isMounted()) {
            return;
        }

        this.getDOMNode().focus();
    }
});

module.exports = Textbox;
