/**
 * @jsx React.DOM
 */
var React = require('react');
var Utilities = require('../utilities');

var TreeNode = React.createClass({
    getInitialState: function () {
        return {
            collapsed : !!this.props.defaultCollapsed
        };
    },

    render: function() {
        var props = this.props;
        var nodeData = this.props.nodeData;

        var text = props.text;
        if (nodeData) {
            text = nodeData.text;
        }

        var level = props.level || 0;
        var children = props.children;
        if (nodeData) {
            children = [];
            var dataStore = nodeData.store;
            dataStore.forEachChildNodeData(nodeData, function (childNodeData) {
                children.push(<TreeNode key={childNodeData.id} nodeData={childNodeData} treeView={props.treeView} level={level + 1} />);
            });
            props.children = children;
        } else {
            children.forEach(function (child) {
                child.props.level = level + 1;
                child.props.treeView = props.treeView;
            });
        }
        this.childTreeNodes = children;

        var childrenNotLoaded = props.childrenNotLoaded;
        if (nodeData) {
            props.childrenNotLoaded = childrenNotLoaded = nodeData.childrenNotLoaded;
        }

        var collapsed = this.isCollapsed();
        var className = 'tree-node';
        var treeNodeBodyClassName = 'tree-node-body';
        var treeNodeChildrenClassName = 'tree-node-children';

        if (this.isActiveNode()) {
            className += ' tree-node-active';
            treeNodeBodyClassName += ' tree-node-body-active';
            treeNodeChildrenClassName += ' tree-node-children-active';
        }

        var expanderText;
        if (this.isLeafNode()) {
            className += ' tree-node-leaf';
            treeNodeBodyClassName += ' tree-node-body-leaf';
            treeNodeChildrenClassName += ' tree-node-children-leaf';
            expanderText = '◦';
        } else {
            if (collapsed) {
                className += ' tree-node-collapsed';
                treeNodeBodyClassName += ' tree-node-body-collapsed';
                treeNodeChildrenClassName += ' tree-node-children-collapsed';
                expanderText = '▸';
            } else {
                expanderText = '▾';
            }
        }

        var hideTopNode = props.treeView.props.hideTopNode;
        if (level === 0) {
            treeNodeBodyClassName += ' tree-node-body-top';
            if (hideTopNode) {
                treeNodeBodyClassName += ' tree-node-body-top-hide';
            }
        }

        var nodeIndentComponents = [];
        var indentCount = level;
        if (hideTopNode) {
            indentCount -= 1;
        }
        for (var i = 0; i < indentCount; i++) {
            nodeIndentComponents.push(<div className="tree-node-indent" />);
        }

        return (
            <div className={className}>
                <div className={treeNodeBodyClassName} onClick={this.handleNodeBodyClick}>
                    <div className="tree-node-indents">{nodeIndentComponents}</div>
                    <div className="tree-node-expander" onClick={this.handleNodeExpanderClick}>{expanderText}</div>
                    <div className="tree-node-content">
                        <span className="tree-node-label">{text}</span>
                    </div>
                </div>
                <div className={treeNodeChildrenClassName}>{children}</div>
            </div>
        );
    },

    handleNodeBodyClick : function (event) {
        var treeView = this.props.treeView;
        treeView.setActiveNode(this);
        if (this.props.onClick) {
            this.props.onClick(this);
        }
        treeView.notifyNodeClick(this);
    },

    handleNodeExpanderClick : function (event) {
        var props = this.props;

        if (!this.isLeafNode()) {
            var collapsed = this.isCollapsed();
            if (collapsed) {
                this.expand();
            } else {
                this.collapse();
            }
        }
        event.stopPropagation();
    },

    expand: function () {
        if (this.isExpanded()) {
            return;
        }

        var props = this.props;
        var treeView = props.treeView;

        var args = {};
        if (props.beforeExpand) {
            props.beforeExpand(this, args);
            if (args.cancel) {
                return;
            }
        }
        treeView.notifyBeforeNodeExpand(this, args);
        if (args.cancel) {
            return;
        }

        var nodeData = props.nodeData;
        if (nodeData) {
            nodeData.collapsed = false;
        }
        this.setState({
            collapsed : false
        });

        if (props.onExpand) {
            props.onExpand(this);
        }
        treeView.notifyNodeExpand(this);
    },

    collapse: function (recursive) {
        if (this.isCollapsed()) {
            return;
        }

        var props = this.props;
        var treeView = props.treeView;

        var args = {};
        if (props.beforeCollapse) {
            props.beforeCollapse(this, args);
            if (args.cancel) {
                return;
            }
        }
        treeView.notifyBeforeNodeCollapse(this, args);
        if (args.cancel) {
            return;
        }

        var nodeData = props.nodeData;
        if (nodeData) {
            nodeData.setCollapsed(true, recursive);
        }
        this.setState({
            collapsed : true
        });

        if (props.onCollapse) {
            props.onCollapse(this);
        }
        treeView.notifyNodeCollapse(this);
    },

    isExpanded: function () {
        return !this.isCollapsed();
    },

    isCollapsed: function () {
        var collapsed = this.state.collapsed;
        var nodeData = this.props.nodeData;
        if (nodeData) {
            collapsed = nodeData.collapsed;
        }
        return collapsed;
    },

    isLeafNode : function () {
        var props = this.props;
        return !props.childrenNotLoaded && (!props.children || !props.children.length);
    },

    isActiveNode : function () {
        return this.props.treeView.getActiveNode() === this;
    },

    getText : function () {
        var props = this.props;
        var text = props.text;
        var nodeData = props.nodeData;
        if (nodeData) {
            text = nodeData.text;
        }
        return text;
    }
});

module.exports = TreeNode;
