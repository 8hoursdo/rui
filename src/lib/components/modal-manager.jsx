/**
 * @jsx React.DOM
 */
var React = require('react');
var Dom = require('../dom');
var utilities = require('../utilities');
var global = require('../global');
var Overlay = require('./overlay.jsx');

var theModalManager = null;

var ModalManager = React.createClass({
    getInitialState: function () {
        return {
            modals: []
        };
    },

    render: function() {
        return (
            <div className="modal-manager">
                <Overlay ref="overlay" />
            </div>
        );
    },

    componentDidMount: function () {
        theModalManager = this;
    },

    componentWillUnmount: function () {
        if (theModalManager === this) {
            theModalManager = null;
        }
    },

    showOverlay: function () {
        return this.refs.overlay.show();
    },

    hideOverlay: function () {
        this.refs.overlay.hide();
    },

    showModal: function (modal, options, callback) {
        var zIndex = this.showOverlay();
        options.zIndex = zIndex + 1;
        modal.show(options, function () {
            var left = options.left;
            var top = options.top;

            var modalDOMNode = modal.getDOMNode();
            if (utilities.isUndefined(left)) {
                var clientWidth = Dom.clientWidth();
                var modalWidth = modalDOMNode.offsetWidth;
                var scrollLeft = document.documentElement.scrollLeft;
                left = (clientWidth - modalWidth) * 0.5 + scrollLeft;
            }

            if (utilities.isUndefined(top)) {
                var clientHeight = Dom.clientHeight();
                var modalHeight = modalDOMNode.offsetHeight;
                var scrollTop = document.documentElement.scrollTop;
                top = (clientHeight - modalHeight) * 0.4 + scrollTop;
            }

            if (left < 0) {
                left = 0;
            }

            if (top < 0) {
                top = 0;
            }

            var nextState = {
                left: left,
                top: top
            }
            modal.setState(nextState, callback);
        });
    }
});

ModalManager.showModal = function (modal, options) {
    if (!theModalManager) {
        throw new Error("Can't show modal. You must put a ModalManager component in your components.");
    }

    return theModalManager.showModal(modal, options);
};

ModalManager.closeModal = function (modal) {
    var closed = modal.close();
    if (closed === false) {
        return;
    }

    if (theModalManager) {
        theModalManager.hideOverlay();
    }
};

module.exports = ModalManager;
