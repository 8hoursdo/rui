/**
 * @jsx React.DOM
 */
var React = require('react');

var FormGroup = React.createClass({
    render: function() {
        var props = this.props;
        var className = 'form-group';

        if (props.stacked) {
            className += ' form-group-stacked';
        }

        if (props.inline) {
            className += ' form-group-inline';
        }

        if (props.nolabel) {
            className += ' form-group-nolabel';
        }

        if (props.compact) {
            className += ' form-group-compact';
        }

        if (props.first) {
            className += ' form-group-first';
        }

        return (
            <div className={className}>
                <label className="form-label">{props.label}</label>
                <div className="form-controls">
                    {props.children}
                </div>
            </div>
        );
    }
});

module.exports = FormGroup;
