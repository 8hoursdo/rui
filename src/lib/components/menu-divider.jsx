/**
 * @jsx React.DOM
 */
var React = require('react');

var MenuDivider = React.createClass({
    render: function() {
        return (
            <li className="menu-divider"></li>
        );
    }
});

module.exports = MenuDivider;
