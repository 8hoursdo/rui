/**
 * @jsx React.DOM
 */
var React = require('react');

var Checkbox = React.createClass({
    getInitialState: function () {
        return {
            checked: !!this.props.defaultChecked
        };
    },

    render: function() {
        var props = this.props,
            name = props.name,
            size = props.size,
            readOnly = !!props.readOnly,
            disabled = !!props.disabled,
            className = 'chk';

        if (size === 'large') {
            className += ' chk-lg';
        }

        if (size === 'small') {
            className += ' chk-sm';
        }

        if (readOnly) {
            className += ' chk-readonly';
        }

        if (disabled) {
            className += ' chk-disabled';
        }

        return (
            <div className={className}>
                <label>
                    <input type="checkbox" name={name} checked={this.state.checked} readOnly={readOnly} disabled={disabled} value={props.value} onChange={this.handleChange} />
                    <span className="chk-text">{props.text}</span>
                </label>
            </div>
        );
    },

    handleChange: function (event) {
        if (this.props.readOnly) {
            return;
        }

        this.setState({
            checked : event.target.checked
        });
    }
});

module.exports = Checkbox;
