/**
 * @jsx React.DOM
 */
var React = require('react');
var utilities = require('../utilities');

var Menu = React.createClass({
    getInitialState: function () {
        var props = this.props;
        var visible = true;
        if (!utilities.isUndefined(props.defaultVisible)) {
            visible = props.defaultVisible;
        }

        var top = 0;
        if (!utilities.isUndefined(props.defaultTop)) {
            top = props.defaultTop;
        }

        var left = 0;
        if (!utilities.isUndefined(props.defaultLeft)) {
            left = props.defaultLeft;
        }

        return {
            visible: visible,
            top: top,
            left: left
        };
    },

    render: function() {
        var me = this;
        var props = this.props;
        var children = props.children;

        if (children && children.length) {
            children.forEach(function (child) {
                child.props.menu = me;
            });
        }

        var state = this.state;
        var style = {
            top: state.top || 0,
            left: state.left || 0
        };
        if (!this.state.visible) {
            style.display = 'none';
        }

        return (
            <div className="menu" style={style} onMouseDown={this.handleMouseDown}>
                <div className="menu-items">
                    {children}
                </div>
            </div>
        );
    },

    handleMouseDown: function (event) {
        event.stopPropagation();
    },

    notifyMenuItemClick: function (menuItem) {
        this.setState({
            visible: false
        });

        if (this.props.onItemClick) {
            this.props.onItemClick(this, {
                item: menuItem
            });
        }
    },

    popup: function (options) {
        var state = {
            visible: true
        };

        if (!utilities.isUndefined(options.top)) {
            state.top = options.top;
        }
        if (!utilities.isUndefined(options.left)) {
            state.left = options.left;
        }

        this.setState(state);
    },

    close: function () {
        this.setState({
            visible: false
        });
    }
});

module.exports = Menu;
