/**
 * @jsx React.DOM
 */
var React = require('react');

var FormDivider = React.createClass({
    render: function() {
        return (
            <div className="form-divider"></div>
        );
    }
});

module.exports = FormDivider;
