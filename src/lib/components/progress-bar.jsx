/**
 * @jsx React.DOM
 */
var React = require('react');
var PropTypes = React.PropTypes;

function getPercentage(options) {
    var delta = options.max - options.min;
    if (delta) {
        return Math.floor(options.value * 100 / delta);
    }

    return 0;
}

function getIndicateText(options) {
    switch (options.indicator) {
        case 'none':
            return '';
        case 'percentage':
            return options.percentage + '%';
        case 'progress':
            return options.value + ' / ' + options.max;
        case 'text':
            var text;
            if (options.percentage === 0) {
                text = options.preparingText;
            } else if (options.percentage === 100) {
                text = options.completedText;
            } else {
                text = options.processingText;
            }
            return text;
    }

    return '';
}

var ProgressBar = React.createClass({
    propTypes : {
        min : PropTypes.number,
        max : PropTypes.number,
        value : PropTypes.number,
        indicator : PropTypes.string
    },

    getDefaultProps : function () {
        return {
            min : 0,
            max : 100,
            value : 0,
            indicator : 'percentage'
        }
    },

    render: function() {
        var props = this.props;

        props.percentage = getPercentage(props);
        var uncompletedPercentage = 100 - props.percentage;
        var indicateText = getIndicateText(props);

        var className = 'progress-bar';

        return (
            <div className={className}>
                <div className="progress-bar-bg">{indicateText}</div>
                <div className="progress-bar-inner" style={{marginLeft: (uncompletedPercentage * -1) + '%'}}>
                    <div className="progress-bar-done" style={{marginLeft: uncompletedPercentage + '%'}}></div>
                    <div className="progress-bar-text" style={{marginLeft: uncompletedPercentage + '%'}}>{indicateText}</div>
                </div>
            </div>
        );
    }
});

module.exports = ProgressBar;
