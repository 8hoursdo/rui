/**
 * @jsx React.DOM
 */
var React = require('react');

var Form = React.createClass({
    render: function() {
        var className = 'form';
        if (this.props.modal) {
            className += ' form-modal';
        }

        if (this.props.narrowLabel) {
            className += ' form-narrow-label';
        }

        if (this.props.stackedLabel) {
            className += ' form-stacked-label';
        }

        return (
            <form {...this.props} className={className} onSubmit={this.handleSubmit}>
                {this.props.children}
            </form>
        );
    },

    handleSubmit: function (event) {
        if (this.props.onSubmit) {
            this.props.onSubmit(this, {event: event});
        }
    }
});

module.exports = Form;
