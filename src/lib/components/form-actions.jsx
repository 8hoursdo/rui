/**
 * @jsx React.DOM
 */
var React = require('react');

var FormActions = React.createClass({
    render: function() {
        var className = 'form-actions';
        var align = this.props.align;
        if (align === 'center') {
            className += ' form-actions-align-center';
        } else if (align === 'right') {
            className += ' form-actions-align-right';
        } else if (align === 'left') {
            className += ' form-actions-align-left';
        }
        return (
            <div className={className}>
                {this.props.children}
            </div>
        );
    }
});

module.exports = FormActions;
