/**
 * @jsx React.DOM
 */
var React = require('react');

var knownBrands = ['default', 'primary', 'success', 'info', 'warning', 'danger'];

var Button = React.createClass({
    render: function() {
        var props = this.props,
            type = props.type || 'button',
            brand = props.brand || (type === 'submit' ? 'primary' : 'default'),
            size = props.size,
            disabled = !!props.disabled,
            href = props.href,
            className = 'btn';

        if (knownBrands.indexOf(brand) !== -1) {
            className += ' btn-' + brand;
        }

        if (size === 'large') {
            className += ' btn-lg';
        }

        if (size === 'small') {
            className += ' btn-sm';
        }

        if (size === 'mini') {
            className += ' btn-xs';
        }

        if (props.active) {
            className += ' active';
        }

        if (props.disabled) {
            className += ' disabled';
        }

        if (href) {
            // render as anchor
            return (
                <a {...this.props} className={className} href={href}>{props.text}</a>
            );
        } else {
            // render as button
            return (
                <button {...this.props} className={className} disabled={disabled}>{props.text}</button>
            );
        }
    }
});

module.exports = Button;
