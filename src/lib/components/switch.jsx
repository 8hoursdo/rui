/**
 * @jsx React.DOM
 */
var React = require('react');

var Switch = React.createClass({
    getDefaultProps: function () {
        return {
            onText: 'YES',
            offText: 'NO'
        }
    },

    getInitialState: function () {
        return {
            checked: !!this.props.defaultChecked
        };
    },

    render: function() {
        var props = this.props,
            className = 'switch';

        if (this.state.checked) {
            className += ' switch-checked';
        }
        if (props.disabled) {
            className += ' switch-disabled';
        }

        return (
            <div className={className} onClick={this.handleClick}><a href="javascript:;"><span className="switch-on">{props.onText}</span><span className="switch-btn"></span><span className="switch-off">{props.offText}</span></a></div>
        );
    },

    handleClick: function (event) {
        if (this.props.disabled) {
            return;
        }

        this.setState({
            checked: !this.state.checked
        });
    }
});

module.exports = Switch;
