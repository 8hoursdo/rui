/**
 * @jsx React.DOM
 */
var React = require('react');
var utilities = require('../utilities');
var global = require('../global');

var Application = React.createClass({
    getInitialState: function () {
        return {};
    },

    componentDidMount: function () {
        global.setTheApplication(this);
    },

    render: function() {
        return (
            <div className="application">
                {this.props.children}
            </div>
        );
    }
});

module.exports = Application;
