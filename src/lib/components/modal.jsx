/**
 * @jsx React.DOM
 */
var React = require('react');
var utilities = require('../utilities');
var ModalManager = require('./modal-manager.jsx');
var ModalFooter = require('./modal-footer.jsx');
var Form = require('./form.jsx');

var Modal = React.createClass({
    getInitialState: function () {
        var props = this.props;
        return {
            visible: props.defaultVisible || false,
            width: props.defaultWidth || 'auto',
            height: props.defaultHeight || 'auto',
            left: props.defaultLeft || 0,
            top: props.defaultTop || 0,
            zIndex: props.defaultZIndex || 2400
        };
    },

    render: function() {
        var state = this.state;
        var style = {
            width: state.width,
            height: state.height,
            top: state.top,
            left: state.left,
            zIndex: state.zIndex
        };

        if (!this.state.visible) {
            style.display = 'none';
        }

        var header = (
            <div key="header" className="modal-header">
                <button type="button" className="close" onClick={this.handleCloseBtnClick}>×</button>
                <h4 className="modal-title">{this.props.title}</h4>
            </div>
        );
        var bodyChildren = [];
        var footer = null;
        React.Children.forEach(this.props.children, function (child) {
            if (child.type === ModalFooter.type) {
                footer = child;
                footer.props.key = 'footer';
            } else {
                if (child.type === Form.type) {
                    if (utilities.isUndefined(child.props.modal)) {
                        child.props.modal = true;
                    }
                }
                bodyChildren.push(child);
            }
        });

        var body = (
            <div key="body" className="modal-body">
                <div className="modal-body-inner">
                    {bodyChildren}
                </div>
            </div>
        );

        var modalChildren = [header, body];
        if (footer) {
            modalChildren.push(footer);
        }

        return (
            <div className="modal-dialog" style={style} tabIndex={-1} onKeyDown={this.handleKeyDown}>
                {modalChildren}
            </div>
        );
    },

    handleKeyDown: function (event) {
        if (event.key === 'Escape') {
            this.close();
        }
        event.stopPropagation();
    },

    handleCloseBtnClick: function (event) {
        event.stopPropagation();
        this.close();
    },

    show: function (options, callback) {
        if (this.state.visible) {
            return;
        }

        if (!options) {
            options = {};
        }

        var state = {
            visible: true,
        };

        if (!utilities.isUndefined(options.zIndex)) {
            state.zIndex = options.zIndex;
        }

        var self = this;
        this.setState(state, function () {
            callback();

            if (self.props.onActive) {
                self.props.onActive(self);
            }
        });
    },

    place: function (options, callback) {
        var state = {};

        if (!utilities.isUndefined(options.top)) {
            state.top = options.top;
        }
        if (!utilities.isUndefined(options.left)) {
            state.left = options.left;
        }
        this.setState(state, callback);
    },

    showModal: function (options, callback) {
        if (!options) {
            options = {};
        }

        this._showAsModal = true;
        ModalManager.showModal(this, options, callback);
    },

    close: function () {
        if (!this.state.visible) {
            return;
        }

        if (this._showAsModal) {
            this._showAsModal = false;
            ModalManager.closeModal(this);
            return;
        }

        var state = {
            visible: false
        };
        this.setState(state);
        if (this.props.onClose) {
            this.props.onClose(this);
        }
    }
});

module.exports = Modal;
