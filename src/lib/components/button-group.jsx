/**
 * @jsx React.DOM
 */
var React = require('react');
var Button = require('./button');

var ButtonGroup = React.createClass({
    render: function() {
        var size = this.props.size,
            className = 'btn-group';

        if (size === 'large') {
            className += ' btn-group-lg';
        }

        if (size === 'small') {
            className += ' btn-group-sm';
        }

        if (size === 'mini') {
            className += ' btn-group-xs';
        }

        return (
            <div className={className}>
                {this.props.children}
            </div>
        );
    }
});

module.exports = ButtonGroup;
