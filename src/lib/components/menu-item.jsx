/**
 * @jsx React.DOM
 */
var React = require('react');

var MenuItem = React.createClass({
    render: function() {
        return (
            <li className="menu-item">
                <a onClick={this.handleMenuItemClick}>
                    <i></i>
                    {this.props.text}
                </a>
            </li>
        );
    },

    handleMenuItemClick: function (event) {
        if (this.props.onClick) {
            this.props.onClick(this);
        }
        event.preventDefault();
        event.stopPropagation();

        this.props.menu.notifyMenuItemClick(this);
    }
});

module.exports = MenuItem;
