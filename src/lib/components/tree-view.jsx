/**
 * @jsx React.DOM
 */
var React = require('react');
var TreeNode = require('./tree-node.jsx');

var TreeView = React.createClass({
    getInitialState: function () {
        return {
            hideTopNode : this.props.hideTopNode,
            dataStore : this.props.dataStore
        };
    },

    render: function() {
        var topNodeData = this.state.dataStore.getTopNodeData();
        return (
            <div className="tree-view">
                <TreeNode ref="topTreeNode" key={topNodeData.id} nodeData={topNodeData} treeView={this} level={0} />
            </div>
        );
    },

    getTopTreeNode: function() {
        return this.refs.topTreeNode;
    },

    getActiveNode: function () {
        return this.activeNode;
    },

    setActiveNode: function (activeNode) {
        if (this.activeNode === activeNode) {
            return;
        }

        var oldActiveNode = this.activeNode;
        this.activeNode = activeNode;
        if (oldActiveNode) {
            oldActiveNode.forceUpdate();
        }
        if (activeNode) {
            activeNode.forceUpdate();
        }

        if (this.props.onActiveNodeChange) {
            this.props.onActiveNodeChange(this, {
                activeNode: activeNode,
                oldActiveNode: oldActiveNode
            });
        }
    },

    notifyNodeClick: function (node) {
        if (this.props.onNodeClick) {
            this.props.onNodeClick(this, {
                node: node
            });
        }
    },

    notifyBeforeNodeCollapse: function (node, args) {
        if (this.props.beforeNodeCollapse) {
            var evtArgs = {
                    node: node
                };
            this.props.beforeNodeCollapse(this, evtArgs);
            args.cancel = evtArgs.cancel;
        }
    },

    notifyNodeCollapse: function (node) {
        if (this.props.onNodeCollapse) {
            this.props.onNodeCollapse(this, {
                node: node
            });
        }
    },

    notifyBeforeNodeExpand: function (node, args) {
        if (this.props.beforeNodeExpand) {
            var evtArgs = {
                    node: node
                };
            this.props.beforeNodeExpand(this, evtArgs);
            args.cancel = evtArgs.cancel;
        }
    },

    notifyNodeExpand: function (node) {
        if (this.props.onNodeExpand) {
            this.props.onNodeExpand(this, {
                node: node
            });
        }
    }
});

module.exports = TreeView;
