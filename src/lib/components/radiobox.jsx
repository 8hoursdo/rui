/**
 * @jsx React.DOM
 */
var React = require('react');

var _radioboxesGroupByName = {};

function addRadioboxIntoGroup(radiobox) {
    var name = radiobox.props.name;
    if (!name) {
        return;
    }

    var radioboxes = _radioboxesGroupByName[name];
    if (!radioboxes) {
        _radioboxesGroupByName[name] = radioboxes = [];
    }

    radioboxes.push(radiobox);
}

function removeRadioboxFromGroup(radiobox) {
    var name = radiobox.props.name;
    if (!name) {
        return;
    }

    var radioboxes = _radioboxesGroupByName[name];
    if (!radioboxes) {
        return;
    }

    var indexOfRadiobox = radioboxes.indexOf(radiobox);
    if (indexOfRadiobox === -1) {
        return;
    }

    radioboxes.splice(indexOfRadiobox, 1);
}

function getOtherCheckedRadiobox(radiobox) {
    var name = radiobox.props.name;
    if (!name) {
        return null;
    }

    var radioboxes = _radioboxesGroupByName[name];
    if (!radioboxes) {
        return null;
    }

    for(var i = 0; i < radioboxes.length; i++) {
        var eachRadiobox = radioboxes[i];
        if (eachRadiobox.state.checked && eachRadiobox !== radiobox) {
            return eachRadiobox;
        }
    }
    return null;
}

var Radiobox = React.createClass({
    getInitialState: function () {
        return {
            checked: !!this.props.defaultChecked
        };
    },

    componentDidMount: function () {
        addRadioboxIntoGroup(this);
    },

    componentWillUnmount: function () {
        removeRadioboxFromGroup(this);
    },

    render: function() {
        var props = this.props,
            name = props.name,
            size = props.size,
            readOnly = !!props.readOnly,
            disabled = !!props.disabled,
            className = 'rdo';

        if (size === 'large') {
            className += ' rdo-lg';
        }

        if (size === 'small') {
            className += ' rdo-sm';
        }

        if (readOnly) {
            className += ' rdo-readonly';
        }

        if (disabled) {
            className += ' rdo-disabled';
        }

        console.log('rendering: checked=' + this.state.checked);
        return (
            <div className={className}>
                <label>
                    <input type="radio" name={name} checked={this.state.checked} readOnly={readOnly} disabled={disabled} value={props.value} onChange={this.handleChange} onClick={this.handleClick} />
                    <span className="rdo-text">{props.text}</span>
                </label>
            </div>
        );
    },

    handleChange: function (event) {
        var props = this.props,
            name = props.name,
            readOnly = props.readOnly;
        var checked = event.target.checked;
        if (readOnly) {
            checked = this.state.checked;
        }

        if (name && checked) {
            var otherCheckedbox = getOtherCheckedRadiobox(this);
            if (otherCheckedbox) {
                console.log('other checked box found');
                otherCheckedbox.setState({checked : false});
            }
        }

        this.setState({
            checked : checked
        });

        return
        if (readOnly) {
            this.forceUpdate();
        }
    },

    handleClick: function (event) {
    }
});

module.exports = Radiobox;
