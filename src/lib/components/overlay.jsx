/**
 * @jsx React.DOM
 */
var React = require('react');
var utilities = require('../utilities');

var MIN_OVERLAY_Z_INDEX = 2300;

var Overlay = React.createClass({
    getInitialState: function () {
        var zIndex = this.props.defaultZIndex || MIN_OVERLAY_Z_INDEX;
        return {
            zIndex: zIndex
        };
    },

    render: function() {
        var zIndex = this.state.zIndex;
        var style = {
            zIndex: this.state.zIndex
        };

        if (zIndex === MIN_OVERLAY_Z_INDEX) {
            style.display = 'none';
        }

        return (
            <div className="overlay" style={style} onClick={this.handleClick}></div>
        );
    },

    handleClick: function (event) {
        if (this.props.onClick) {
            this.props.onClick(this);
        }
        event.stopPropagation();
    },

    show: function () {
        var state = {
            zIndex: this.state.zIndex + 2
        };
        this.setState(state);
        return state.zIndex;
    },

    hide: function () {
        var zIndex = this.state.zIndex;
        if (zIndex === MIN_OVERLAY_Z_INDEX) {
            return;
        }

        var state = {
            zIndex: zIndex - 2
        };
        this.setState(state);
    }
});

module.exports = Overlay;
