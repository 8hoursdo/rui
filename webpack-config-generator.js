var path = require('path');
var webpack = require('webpack');

var libRoot = path.join(__dirname, 'src/lib');
var nodeRoot = path.join(__dirname, 'node_modules');


exports.generate = function () {
    var plugins = [];

    var buildSettingsGroup = {
        default: {
            outputFileName: 'rui.js',
            outputLibraryTarget: 'var',
            reactExternalName : 'React'
        },
        commonjs: {
            outputFileName: 'rui.commonjs.js',
            outputLibraryTarget: 'commonjs2',
            reactExternalName : 'react'
        }
    };

    var buildSettings;
    if (process.env.WEBPACK_BUILD_TARGET === 'commonjs') {
        buildSettings = buildSettingsGroup.commonjs;
    } else {
        buildSettings = buildSettingsGroup.default;
    }

    return {
        entry: 'rui',
        output: {
            library: 'rui',
            libraryTarget: buildSettings.outputLibraryTarget,
            path: path.resolve('./dist/js'),
            publicPath: '/dist/',
            filename: buildSettings.outputFileName,
            sourceMapFilename: '[file].map'
        },
        devtool: 'source-map',
        externals: {
            react: buildSettings.reactExternalName
        },
        resolve: {
            root: libRoot,
            extensions: ['', '.js', '.jsx']
        },
        resolveLoader: {
            root: nodeRoot
        },
        plugins: plugins,
        module: {
            loaders: [
                {test: /\.jsx/, loader: 'jsx'}
            ]
        }
    };
};

