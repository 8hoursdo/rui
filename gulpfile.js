var gulp = require('gulp');
var changed = require('gulp-changed');
var stylus = require('gulp-stylus');
var webpack = require('gulp-webpack');
var webpackConfigGenerator = require('./webpack-config-generator');
var env = require('gulp-env');


gulp.task('default', ['style', 'script'], function() {
});

gulp.task('watch', ['default'], function () {
    gulp.watch('src/style/**/*.styl', ['style']);
    gulp.watch('src/lib/**/*.*', ['script']);
});

gulp.task('style', function () {
    return gulp.src('src/style/rui.styl')
        .pipe(stylus())
        .pipe(gulp.dest('dist/css'));
});

gulp.task('script', ['script-default', 'script-commonjs']);

gulp.task('script-default', function () {
    env({
        vars: {
            WEBPACK_BUILD_TARGET: 'default'
        }
    });
    return gulp.src('src/lib/rui.js')
        .pipe(webpack(webpackConfigGenerator.generate()))
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('script-commonjs', ['script-default'], function () {
    env({
        vars: {
            WEBPACK_BUILD_TARGET: 'commonjs'
        }
    });
    return gulp.src('src/lib/rui.js')
        .pipe(webpack(webpackConfigGenerator.generate()))
        .pipe(gulp.dest('dist/js/'));
});
